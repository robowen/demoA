package com.rooooo.thread;

import org.openjdk.jol.info.ClassLayout;

import java.util.concurrent.atomic.AtomicInteger;

public class T01 {
    public static void main(String[] args) {
       /* AtomicInteger i = new AtomicInteger();
        i.incrementAndGet();*/

        Object o = new Object();
        System.out.println(ClassLayout.parseInstance(o).toPrintable());
        synchronized (o){
            System.out.println(ClassLayout.parseInstance(o).toPrintable());
        }
    }
}
