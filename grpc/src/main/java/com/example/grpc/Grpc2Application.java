package com.example.grpc;

import com.example.grpc.factory.GrpcServerFactory;
import io.grpc.Server;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@Slf4j
public class Grpc2Application {

	private static final Logger LOGGER = LogManager.getLogger(Grpc2Application.class);

	public static void main(String[] args) throws Exception{
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext-proxy.xml");
		GrpcServerFactory serverFactory = context.getBean(GrpcServerFactory.class);
		Server server = serverFactory.createServer();
		server.start();
		server.awaitTermination();
		LOGGER.info("1");

	}

}
